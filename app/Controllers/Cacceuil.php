<?php

namespace App\Controllers;
use CodeIgniter\Controller;


class Cacceuil extends Controller
{
	public function index()
	{
		$data['page_title'] = "Concours photographique des Dev Web";
		
		$page['contenu'] = view('v_acceuil', $data);
		return view('commun/v_template', $page);
	}
}
