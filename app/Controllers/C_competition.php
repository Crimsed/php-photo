<?php

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\M_competition;
use CodeIgniter\Exceptions\PageNotFoundException;


class C_competition extends Controller
{
	public function index()
	{
        $model = new M_competition();
        $data['result'] = $model->getAll();
		$data['page_title'] = "Concours photographique des Dev Web";
        $data['titre1'] = "la liste des compétitions";
		
		$page['contenu'] = view('competition/V_liste_competition', $data);
		return view('commun/v_template', $page);
	}

    public function detail($prmId = null)
	{

		if ($prmId != null) {
			$model = new M_competition();
            $data['resultPhoto'] = $model->select_all_by_id($prmId);
			$data['resultCompetition'] = $model->select_all($prmId);
			if (count($data['resultCompetition']) != 0) {
				$data['titre1'] = "La compétition ";
				$data['page_title'] = "La liste des compétitions";

				$page['contenu'] = view('competition/V_detail_competition', $data);
				return view('commun/v_template', $page);
			} else {
				throw PageNotFoundException::forPageNotFound("Ce conteneur n'existe pas !");
			}
		} else{
			throw PageNotFoundException::forPageNotFound("Il faut choisir un conteneur !");
		}
	}


}
