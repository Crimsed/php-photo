<?php

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\M_photo;

class C_photo extends Controller
{
	public function detail($prmId)
	{
		$model = new M_photo();
		$data['resultImage'] = $model->select_detail_by_id($prmId);
		$data['page_title'] = "Photo";
		
		$page['contenu'] = view('photo/V_detail_photo', $data);
		return view('commun/v_template', $page);
	}
}
