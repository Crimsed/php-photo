<?php 
namespace app\Models;
use CodeIgniter\Model;

class M_photo extends Model {

    protected $table = 'photo';
    protected $primarykey = 'id';
    protected $returntype = 'array';

    public function select_detail_by_id($prmId) {
        $requete = $this->select('*')
                        ->where(['Id' => $prmId]);
        return $requete->findAll();
    }
}