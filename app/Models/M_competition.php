<?php 
namespace app\Models;
use CodeIgniter\Model;

class M_competition extends Model {

    protected $table = 'competition';
    protected $primarykey = 'id';
    protected $returntype = 'array';

    public function getAll() {
        $requete = $this->select('Nom,Date,Id');
        return $requete->findAll();
    }
    public function select_all($prmId)
    {
        $requete = $this->select('*')->where(['Id' => $prmId]); 
        return $requete->findAll();
    }
    public function select_all_by_id($prmId)
    {
        return $this->select('Titre,Classement,Prenom,concurrent.Nom,Pays,photo.Id')
        ->join('photo','competition.Id = photo.competitionId','left')
        ->join('concurrent','concurrent.Id = photo.concurrentId','left')
        ->where(['competitionId' => $prmId])
        ->orderBy('Classement','ASC')
        ->findAll();
    }
}